  <!DOCTYPE html>
<html>
<head>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap.bundle.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link rel="icon" type="image/png" href="img/icono.png"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Inicio</title>
</head>
<body>
	<header>
  	<script src='js/add_product_index.js'></script>
<?php
session_start();
if(!isset($_SESSION['usuario'])){
		//Usuario No Logeado
		include 'index_header.php';
	}
	else{
		//Usuario No Logeado
		include 'index_header_logeado.php';
	}
?>
	<script src="js/buscador_header.js"></script>
	</header>
	<main>
			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
			    	<div class="carousel-item active">
			      		<img src="img/carrusel1.jpg" class="img_carrusel" alt="...">
			    	</div>
			    	<div class="carousel-item">
			      		<img src="img/carrusel2.jpg" class="img_carrusel" alt="...">
			    	</div>
			    	<div class="carousel-item">
			      		<img src="img/carrusel3.jpg" class="img_carrusel" alt="...">
			    	</div>
			  	</div>
			  	<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
			    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    	<span class="sr-only">Anterior</span>
			  	</a>
			  	<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Siguiente</span>
			  	</a>
			</div>
		<div id="producto_target" class="producto_targeta">
			<?php require "backend/home.php";
			foreach ($productos as $producto) { ?>
			<div id="<?php echo $producto['id'] ?>" class="card" style="width: 20rem; height: 30em; float: left; margin: 30px;">
				<a href="producto_seleccionado.php?id=<?php echo $producto['id'] ?>"><img src="img/<?php echo $producto['nombre'] ?>_1.png" class="card-img-top imagen_producto" alt="Esto deberia de ser un producto..."></a>
			  	<div class="card-body">
			    	<h5 class="card-title"><?php echo $producto['nombre'] ?></h5>
			    	<p class="card-text">$<?php echo $producto['precio'] ?></p>
			    	<a href="producto_seleccionado.php?id=<?php echo $producto['id'] ?>" class="btn btn-primary">Ver Producto</a>
			    	<?php if($producto['stock']!=0){ echo "<a id='" .$producto['id']. "' class='agregar' style='margin-left: 35%; margin-top: 15px;' data-placement='bottom' data-toggle='tooltip' title='Añadir al Carrito'><i class='fas fa-cart-plus fa-lg' style='color: #161A20; font-size: 30px;'></i></a>"; } ?>
			  	</div>
			</div>
			<?php } ?>
		</div>
	</main>
	<footer>
		<div id="div"></div>
		<?php //require "backend/paginador.php";    // include 'backend/buscador_header.php'; ?>	
	</footer>
</body>
</html>