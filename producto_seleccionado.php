<!DOCTYPE html>
<html>
<head>
	<?php require "backend/producto_seleccionado.php"; ?>
	<script src="js/jquery.js"></script>
	<script src="js/index_header.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap.bundle.js"></script>
	<script type="text/javascript" src="js/producto_seleccionado.js"></script>
	<!-- <script src="usuario_carrito.js"></script> -->
  	<link rel="icon" type="image/png" href="img/no.png"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<title><?php echo $producto['nombre'];?></title>
</head>
<body>
	<header> <!-- Encabezado -->
	<?php
	session_start();
	if(!isset($_SESSION['usuario'])){
		//Usuario No Logeado
		include 'index_header.php';
	}
	else{
		//Usuario No Logeado
		include 'index_header_logeado.php';
	}
	?>
		<script type="text/javascript" src="js/buscador_header.js"></script>	
	</header>
	<main>	
		<div id="producto_target">	
			<div id="cont" class="media">	
				<div id="img-produc">
					<img id="0" class="img_principal" src="img/<?php echo $producto['nombre']; ?>_1.png">
				</div>
				<div id="descripcion" class="media-body">
					<strong id="name"><?php echo $producto['nombre']; ?></strong><br>
					<input id="stock_val" type="hidden" value="<?php if($producto['stock']>0){ echo "En stock"; }else{echo "Fuera de stock";}  ?>">
					<input type="hidden" id="cantiadad_de_stock" value="<?php echo $producto['stock'] ?>">
					<label id="stock"><?php if($producto['stock']>0){ echo "En stock"; }else{echo "Fuera de stock";}  ?></label>
					<p id="pres">Precio:</p>
					<strong id="valor" >$<?php echo $producto['precio']; ?></strong><br>
					<div id="cantidad">
						<button id='menos' class="variacion-canta-producto"><i class="fas fa-minus"></i></button>
						<input type='number' class='form-control variacion-canta-producto' id='text_cant' name='cant_producto' value='1'>
						<button id='mas' class="variacion-canta-producto"><i class='fas fa-plus'></i></button>
					</div>
					<div id="cont_car" >
					<form method="POST" action="backend/verificacion_cantidad.php" >
						<input type="hidden" name="cant_stock" value="<?php echo $producto['stock'] ?>">
						<input type="hidden" name="id_producto" value="<?php echo $producto['id'] ?>">
						<input type="hidden" name="cantidad_de_productos" id="cant_product_hidden" value="1">
						<button type="submit" class="boton-vacio-producto btn btn-primary btn-lg btn-block" onclick="return confirm('¿Quieres agregar al carrito?')"> Comprar Ahora <i class='fas fa-cart-plus'></i></button>
					</form>
					</div>
					<div id="tabla">			
						<table border="2">
							<tr>
								<td><img class="img" id="1" src="img/<?php echo $producto['nombre']; ?>_2.png"></td>	
								<td><img class="img" id="2" src="img/<?php echo $producto['nombre']; ?>_3.png"></td>
								<td><img class="img" id="3" src="img/<?php echo $producto['nombre']; ?>_4.png"></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div id="informacion">
				<h2><strong>Descripcion:</strong></h2> 
				<h5><?php echo $producto['descripcion']?></h5>
			<div id="cargar_info"></div>
		</div>
	</div>
	</main>
</body>
</html>.
<!-- <i id="carrito" class="fas fa-cart-plus"></i> -->