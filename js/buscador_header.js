pag_actual = 0;

function confirmarCompra(id){
	var confirmar = confirm('¿Quieres agregar al carrito?');
	if (confirmar == true) {
		console.log(id);
			$.ajax({
				url:"backend/add_product_from_index.php", 
				type: 'POST',
				data: {'accion':'agregar', 'id': id}, 
				dataType:"json",
				success:function(res){
					
				}
			});
	}
}

function irA(pagina, cantidad) {
		$.ajax({
        url: "backend/buscador_header.php", //envia
        data: {'dc':$("#dc").val(), 'pag': pagina, 'cantidad' : cantidad}, //captura todo lo que esta en el formulario
        type: "POST",
        dataType: "json", // recibe
        success: function(s){
			var div="";
        	var x=0;
        	console.log(s['usuarios'])
        	if(s['usuarios'].length>0){
	        	div+="<h1 style='text-align: left;'> Resultados para '"+$("#dc").val()+"'</h1>";
	        	s['usuarios'].forEach(function(){
	        		div+="<div id='"+s['usuarios'][x]['id']+"' class='card' style='width: 20rem; height: 30em; float: left; margin: 30px;'>";
	        		div+="<a href='producto_seleccionado.php?id="+s['usuarios'][x]['id']+"'><img src='img/"+s['usuarios'][x]['nombre']+"_1.png' class='card-img-top imagen_producto' alt='...Esto deberia ser un producto'></a>";
	        		div+="<div class='card-body'>";
	        		div+="<h5 class='card-title'>"+s['usuarios'][x]['nombre']+"</h5>";
	        		div+="<p class='card-text'>$"+s['usuarios'][x]['precio']+"</p>";
	        		div+="<a href='producto_seleccionado.php?id="+s['usuarios'][x]['id']+"' class='btn btn-primary'>Ver Producto</a>";
	        		if(s['usuarios'][x]['stock']!=0){
	        			div+="<a id='" +s['usuarios'][x]['id']+ "' class='agregar2' onclick='return confirmarCompra("+s['usuarios'][x]['id']+");'' style='margin-left: 35%; margin-top: 15px;' data-placement='bottom' data-toggle='tooltip' title='Añadir al Carrito'><i class='fas fa-cart-plus fa-lg' style='color: #161A20; font-size: 30px;'></i></a>";	
	        		}
	        		div+="</div>";
	        		div+="</div>";
        			div+="<script src='js/add_product_index.js'></script>"
	        		x++;
	        	}); //
	        	$('#carouselExampleControls').remove();
	        	$('#producto_target').html(div);
        	}else{
        		var not_found= "<h1>No se encontro resultados de '"+$("#dc").val()+"'</h1>";
	        	$('#carouselExampleControls').remove();
        		$('#producto_target').html(not_found);
        	}
        var cant_r = s['cant_registros'][0]; 
              
        		var pag_consiguiente;
				var pag_ini = 1	
		 		var paginador="";
		 		const reg_x_pagina=2 ;//cantidad de registros que se veran en cada pagina
		 		pag_consiguiente != 1;
		 		var itr=1;
		 		var table='<nav aria-label="paginador" style="text-align: center;"><ul class="pagination justify-content-center">';		 		
		 		var total_paginas=Math.ceil(cant_r/reg_x_pagina);//aqui se usa cant_r 🕊️	
				/*for(x=1;x<=1; x++) {
					if(pag_consiguiente != false){
						table+='<td><a onclick="irA('+cantidad+',1)"><i class="fas fa-arrow-right"></i></a></td>';
						table+='<td><a onclick="irA('+(pag_actual+',2)"><i class="fas fa-arrow-circle-right"></i></a></td>';
					}
				}*/
				for(x=1;x<=total_paginas; x++) {
					
						table+='<li class="page-item"><a onclick="irA('+x+',2)" class="page-link" href="#">'+x+'</a></li>';
					
				}/*
				for(x=1;x<=1; x++) {
					if(pagina != total_paginas){
						table+='<td onclick="irA('+(pagina-x)+')"><i class="fas fa-arrow-circle-left"></i></a></td>';
						table+='<td onclick="irA('+pag_ini+',1)"><i class="fas fa-arrow-left"></i></a></td>';
					}				
				}
*/
				table+='</ul></nav>';
				$('#div').html(table);
        }
	});

}
$('#boton-busqueda').click(function(){	 	
		irA('1','2');
	});