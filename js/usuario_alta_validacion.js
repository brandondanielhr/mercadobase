$(document).ready(function(){
    $("#btn").click(function() {
        $.ajax({
            url: "backend/usuario_alta.php/", //envia
            data: $("#formulario").serialize(),//captura todo lo que esta en el formulario
            type: "POST",
            dataType: "json", // recibe
            success:function(res){
                if(res['mensaje'] == 'Ya existe ese usuario.Cambie el Email o Password'){
                    alert(res['mensaje']);
                    return;
                }

                
                var cont =0;
                var arr = new Array();
                arr.push(res);
                console.log(arr);
                
                //nombre
                var nombre="";
                if(res['nombre']=="El maximo es de 35 caracteres" || res['nombre']=="Agrega Tu nombre"){
                    nombre = res['nombre'];
                    $('#err_nombre').addClass('alert alert-dismissible alert-danger');
                    $('#err_nombre').html(nombre);
                    $('#nombre').addClass("is-invalid");
                }else{
                    $('#err_nombre').removeClass('alert alert-dismissible alert-danger');
                    $('#nombre').removeClass("is-invalid");
                    $('#nombre').addClass("is-valid");
                    $('#err_nombre').html("");
                    cont++;
                }

                //apellido
                var apellido="";
                if(res['apellido']=="Agrega Tu Apellido"|| res['apellido']=="El maximo es de 35 caracteres"){
                    apellido = res['apellido'];
                    $('#err_apellido').addClass('alert alert-dismissible alert-danger');
                    $('#err_apellido').html(apellido);
                    $('#apellido').addClass("is-invalid");
                }else{
                    $('#err_apellido').removeClass('alert alert-dismissible alert-danger');
                    $('#apellido').removeClass("is-invalid");
                    $('#apellido').addClass("is-valid");
                    $('#err_apellido').html("");
                    cont++;
                }

                //email
                var email="";
                if(res['email']=="Agrega tu email"||res['email']=="El correo es incorrecto"){
                    email = res['email'];
                    $('#err_email').addClass('alert alert-dismissible alert-danger');
                    $('#err_email').html(email);
                    $('#email').addClass("is-invalid");
                }else{
                    $('#err_email').removeClass('alert alert-dismissible alert-danger');
                    $('#email').removeClass("is-invalid");
                    $('#email').addClass("is-valid");
                    $('#err_email').html("");
                    cont++;
                }

                //calle
                var calle="";
                if(res['calle']=="Es obligatorio completar este campo"){
                    calle = res['calle'];
                    $('#err_calle').addClass('alert alert-dismissible alert-danger');
                    $('#err_calle').html(calle);
                    $('#calle').addClass("is-invalid");
                }else{
                    $('#err_calle').removeClass('alert alert-dismissible alert-danger');
                    $('#calle').removeClass("is-invalid");
                    $('#calle').addClass("is-valid");
                    $('#err_calle').html("");
                    cont++;
                }

                //ciudad
                var ciudad="";
                if(res['ciudad']=="Es obligatorio completar este campo"){
                    ciudad = res['ciudad'];
                    $('#err_ciudad').addClass('alert alert-dismissible alert-danger');
                    $('#err_ciudad').html(ciudad);
                    $('#ciudad').addClass("is-invalid");
                }else{
                    $('#err_ciudad').removeClass('alert alert-dismissible alert-danger');
                    $('#ciudad').removeClass("is-invalid");
                    $('#ciudad').addClass("is-valid");
                    $('#err_ciudad').html("");
                    cont++;
                }

                //telefono
                var telefono="";
                if(res['telefono']== "Agregue un numero de telefono" || res['telefono'] == "El Numero de telefono debe ser mayor a 9 digitos y menor a 13" || res['telefono']=="Numero de telefono invalido"){
                    telefono = res['telefono'];
                    $('#err_telefono').addClass('alert alert-dismissible alert-danger');
                    $('#err_telefono').html(telefono);
                    $('#telefono').addClass("is-invalid");
                }else{
                    $('#err_telefono').removeClass('alert alert-dismissible alert-danger');
                    $('#telefono').removeClass("is-invalid");
                    $('#telefono').addClass("is-valid");
                    $('#err_telefono').html("");
                    cont++;
                }

                //contraseña
                var contrasena="";
                if(res['contrasena']=="Agregue una contraseña"||res['contrasena']=="La contraseña de contener mas de 6 caracteres"){
                    contrasena = res['contrasena'];
                    $('#err_contrasena').addClass('alert alert-dismissible alert-danger');
                    $('#err_contrasena').html(contrasena);
                    $('#contrasena').addClass("is-invalid");
                }else{
                    $('#err_contrasena').removeClass('alert alert-dismissible alert-danger');
                    $('#contrasena').removeClass("is-invalid");
                    $('#contrasena').addClass("is-valid");
                    $('#err_contrasena').html("");
                    cont++;
                }

                //Confirmar Contraseña
                var contrasena_again="";
                if(res['contrasena_again']=="Vuelve a escribir la contraseña de nuevo"||res['contrasena_again']=="Las contraseñas no coinciden"){
                    contrasena_again = res['contrasena_again'];
                    $('#err_contrasena_again').addClass('alert alert-dismissible alert-danger');
                    $('#err_contrasena_again').html(contrasena_again);
                    $('#contrasena_again').addClass("is-invalid");
                }else{
                    $('#err_contrasena_again').removeClass('alert alert-dismissible alert-danger');
                    $('#contrasena_again').removeClass("is-invalid");
                    $('#contrasena_again').addClass("is-valid");
                    $('#err_contrasena_again').html("");
                    cont++;
                }                
                console.log(cont);
                //redireccionamiento 
                if(cont==8){
                    alert('Tu cuenta se creo exitosamente');
                    window.location.href = "http://localhost/mercadobase/usuario_inicio_sesion.php";
                }
            }
        })
    });
   	$('#mostrar').click(function(){
   		if(($('#contrasena').attr('type'))=="Password"){
   			$('#contrasena').attr('type','text');
            console.log("holas");
   		}else{
            if(($('#contrasena').attr('type'))=='text'){
                $('#contrasena').attr('type','Password');
            }
   		}
   	});

}); 
