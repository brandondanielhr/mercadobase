<!DOCTYPE html>
<html>
<head>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap.bundle.js"></script>
	<link rel="icon" type="image/png" href="img/icono.png"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<meta charset="utf-8">
	<title>Listado de Usuarios</title>
</head>
<body>
	
	<?php include "conexiones/conex_bd.php"; ?>
	<?php require "backend/paginador.php"; ?>
	
	<header>
		<?php include 'usuario_header.php' ?>
	</header>
	<main>
		<center>
			<table  class="table table-hover">
				<tr class="table-active">
					<th scope="col">Nombre</th>
					<th scope="col">Apellido</th>
					<th scope="col">Email</th>
					<th scope="col">Telefono</th>
					<th scope="col">Ciudad</th>
					<th scope="col">Calle</th>
					<th colspan="2"><center>Acciones</center></th>
				</tr>
				<?php foreach($usuarios as $usuario){ ?>
				<tr class="table-primary">
					<td><?php echo $usuario['nombre'] ?></td>
					<td><?php echo $usuario['apellido'] ?></td>
					<td><?php echo $usuario['email'] ?></td>
					<td><?php echo $usuario['telefono'] ?></td>
					<td><?php echo $usuario['ciudad'] ?></td>
					<td><?php echo $usuario['calle'] ?></td>
					<td><a href="usuario_editar.php?id=<?php echo $usuario['id'] ?>">Editar</a></td>
					<td><a onclick="return confirm('¿Estas Seguro? El usuario  <?php echo $usuario['nombre'] ?> se eliminara para siempre')" href="backend/usuario_baja_fisica.php?id=<?php echo 
        				$usuario['id'] ?>">Eliminar</a></td>
				</tr>
				<?php } ?>
			</table><br>
		</center>
	</main>
	<footer>
		
	</footer>
	<script src="js/jquery.js"></script>
	<script src="js/usuario_lista.js"></script>
</body>
</html>