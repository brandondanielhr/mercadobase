<?php
session_start();//aqui se trabajara con sesiones mas adelante
?>



<!DOCTYPE html>
<html>
<head>
	<link rel="icon" type="image/png" href="img/no.png"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<meta charset="utf-8">
	<title>Listado de Usuarios</title>
</head>
<body>
	<?php include "conexiones/conex_bd.php"; ?>
	<?php include "backend/usuario_listado.php"; ?>
	

	<?php

	$buscador=strtolower($_REQUEST['buscador']);
	if(empty($buscador)){
		header('location:usuario_listado.php');
	}
	?>
	<!--<header>
		<?php include 'header.php' ?>
	</header> -->
	<form method="GET" action="buscar_cliente.php" >
		<input type="text" name="buscador" id="busqueda" placeholder="Buscar" value="<?php echo $buscador ?>" >
		<input type="submit" value="Buscar">
	</form>

	<main>
		<center>
			<table  class="table table-hover">
				<tr class="table-active">
					<th> <input type="checkbox" id="selec_todo" > </th>
					<th scope="col">ID</th>
					<th scope="col">Nombre</th>
					<th scope="col">Apellido</th>
					<th scope="col">Email</th>
					<th scope="col">Telefono</th>
					<th scope="col">Ciudad</th>
					<th scope="col">Calle</th>
					<th colspan="2"><center>Acciones</center></th>
				</tr>


<?php
$consulta="SELECT COUNT(*) as cant_registros FROM clientes 
									where (nombre like '%$buscador%' or
									id like '%$buscador%')";
									

$respuesta=mysqli_query($conect,$consulta);
	if ($respuesta == false){
		echo mysqli_error($conect);
		die();
}
$cant_registros=mysqli_fetch_assoc($respuesta)['cant_registros'];


$segundo_valor=5;
if(empty($_GET['pagina'])){
	$pagina=1;
}
else{
	$pagina= $_GET['pagina'];
}


$operacion=($pagina-1)*$segundo_valor;

$total_paginas=ceil($cant_registros/$segundo_valor);

$consulta="SELECT * FROM clientes where 
(nombre like '%$buscador%' or id like '%$buscador%') 
LIMIT $operacion,$segundo_valor";




$respuesta=mysqli_query($conect,$consulta);
	if ($respuesta == false){
		echo mysqli_error($conect);
		die();
}
$usuarios=mysqli_fetch_all($respuesta,MYSQLI_ASSOC);
?>



				<?php foreach($usuarios as $usuario){ ?>
				<tr class="table-primary">
					<td><input type="checkbox" class="users" id="<?php echo $usuario['id'] ?>" ></td>
					<td><?php echo $usuario['id'] ?></td>
					<td><?php echo $usuario['nombre'] ?></td>
					<td><?php echo $usuario['apellido'] ?></td>
					<td><?php echo $usuario['email'] ?></td>
					<td><?php echo $usuario['telefono'] ?></td>
					<td><?php echo $usuario['ciudad'] ?></td>
					<td><?php echo $usuario['calle'] ?></td>
					<td><a href="usuario_editar.php?id=<?php echo $usuario['id'] ?>">Editar</a></td>
					<td><a onclick="return confirm('¿Estas Seguro? El usuario  <?php echo $usuario['nombre'] ?> se eliminara para siempre')" href="backend/usuario_baja_fisica.php?id=<?php echo 
        				$usuario['id'] ?>">Eliminar</a></td>
				</tr>
				<?php } ?>
				<div>
				
				<?php
					if($cant_registros > 0){
					if($pagina != 1){	
				?>
				<a href="?pagina=<?php echo 1;?>&buscador=<?php echo $buscador?>">|<</a>
				<a href="?pagina=<?php echo $pagina-1; ?>&buscador=<?php echo $buscador ?>"><<</a>
				
				<?php
				}
				echo "<table><tr>";
				for($x=1; $x <= $total_paginas; $x++) {
					if($x == $pagina){
					echo '<td>'.$x.'</td>';//aqui se le mete una clase para que coloree
					}
					else{
					echo '<td><a href="?pagina='.$x.'&buscador='.$buscador.'">'.$x.'</a></td>';
					}
 				}
				echo "</tr></table>";
				if($pagina != $total_paginas){

				
				?>

				<a href="?pagina=<?php echo $pagina + 1;?>&buscador=<?php echo $buscador ?>">>></a>
				<a href="?pagina=<?php echo $total_paginas?>&buscador=<?php echo $buscador ?>">>|</a>
				<?php } }?>
				
			</div>
			</table>


			</table><br>

			<a href="usuario_alta.php">Aqui ta el botun :v</a> <!-- cosito que servira para mas eficiencia -->
			<a onclick="return confirm('Estas Seguro??')" href="backend/usuario_baja.php" id="borrar_todo"><i class="fas fa-trash-alt"></i></a>
		</center>
	</main>
	<footer>
		
	</footer>
	<script src="js/jquery.js"></script>
	<script src="js/usuario_lista.js"></script>
</body>
</html>