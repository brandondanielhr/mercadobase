<!DOCTYPE html>
<html>
<head>
	<script src="js/jquery.js"></script>
	<link rel="icon" type="image/png" href="img/no.png"/>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
  <script src="js/usuario_recuperacion_clave.js"></script>

	
	<meta charset="utf-8">
	<title>Iniciar Sesion</title>
</head>
<body>
	<header>
		<?php include 'usuario_header.php' ?>
	</header>
	<main>
		<form id="rec_clave" class="form_usuario" method="POST">
			<fieldset>
  				<legend>Ingresa tu Email y Contraseña:</legend>
  				<div class="form-group">
      				<label for="exampleInputEmail1">Email:</label>
      				<input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Escribir email..."
      				>
    			</div>
          
    			<div class="form-group">
      				<label for="exampleInputPassword1">Nueva Contraseña:</label>
      				<input type="Password" name="password" class="form-control" id="nuevaContraseña" placeholder="Contraseña...">
    			</div>

    			<div class="form-group">
      				<label for="exampleInputPassword1">Confirmar Contraseña:</label>
      				<input type="Password" name="confirm-password" class="form-control" id="confirmarNuevaContraseña" placeholder="Confirmar Contraseña...">
    			</div>
  			</fieldset>
  			
  			<center>
        <input type="submit" value="Enviar"  
  			class="btn btn-primary btnform" id="btn">
      </center>
		<!--</form>-->
	</main>
	<footer>
</body>
</html>
<?php