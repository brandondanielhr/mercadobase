<!DOCTYPE html>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="js/usuario_informacion.js"></script>
  <script src="js/bootstrap.js"></script>
  <link rel="icon" type="image/png" href="img/icono.png"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<title>Actualizar Datos</title>
	<meta charset="utf-8">
</head>
<body>
  <?php require 'backend/usuario_informacion.php'; ?>
    <?php include 'index_header_logeado.php'; ?>

	<header>
		<?php include 'usuario_header.php' ?>
	</header>
	<main>    
	<form method="POST" id="act_info" class="form_usuario"> 
			<fieldset>
  				<legend>Actualizar Usuario:</legend>
  				<div class="form-group">
      				<label for="exampleInputEmail1">Email:</label>
      				<input type="email" class="form-control" name="email"  aria-describedby="emailHelp" placeholder="Escribir email..." value=<?php print_r($assoc['email']); ?> >
              <div id="err_email"></div>
    			</div>
    			<div class="form-group">
      				<label for="exampleInputEmail1">Nombre:</label>
      				<input type="text" class="form-control" name="nombre" placeholder="Nombre..." value=<?php print_r($assoc['nombre']); ?>>
              <div id="err_nombre"></div>
    			</div>
    			<div class="form-group">
      				<label for="exampleInputEmail1">Apellido:</label>
      				<input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apellido..." value=<?php print_r($assoc['apellido']); ?>>
              <div id="err_apellido"></div>
    			</div>
    			<div class="form-group">
      				<label for="exampleInputEmail1">Calle:</label>
      				<input type="text" class="form-control" name="calle" id="calle" placeholder="Calle..." value=<?php print_r($assoc['calle']); ?>>
    			</div>
    			<div class="form-group">
      				<label for="exampleInputEmail1">Ciudad:</label>
      				<input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ciudad..." value=<?php print_r($assoc['ciudad']); ?>>
    			</div>
              <div class="form-group">
              <label for="exampleInputEmail1">Telefono:</label>
              <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Telefono..." value=<?php print_r($assoc['telefono']); ?>>
          </div>
                <div><a href="usuario_informacion.php">Cancelar</a></div>
  			</fieldset>
  			<center><input type="button" class="btn btn-primary btnform" id="btn" value="Actualizar Usuario"></input></center>
        </form>
   
	</main>
	<footer>
	</footer>
</body>
</html>