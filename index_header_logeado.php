<meta name="viewport" content="width=device-width, initial-scale=1"> <!-- 👺 -->
<link rel="stylesheet" type="text/css" href="css/index.css">
<div style="position: absolute; width: 100%; height: 106px; z-index: 100;">

	<a href="index.php">
	<div id="icon">
		<div id="icon-mercado">
			<h5 id="mercado"><b class="mercado-h">M</b><b class="mercado-h">e</b><b class="mercado-h">r</b><b class="mercado-h">c</b><b class="mercado-h">a</b><b class="mercado-h">d</b><b class="mercado-h">o</b></h5>
		</div>
		<div id="icon-base">
			<h5 id="base"><b class="base-h">B</b><b class="base-h">a</b><b class="base-h">s</b><b class="base-h">e</b></h5>
		</div>
	</div>
	</a>
	<div id="busqueda">
		<input type="search" id="dc">
		<button id="boton-busqueda" data-placement="bottom" data-toggle="tooltip" title="Buscar">
			<i class="fas fa-search fa-lg" style="color: #161A20; font-size: 25px;"></i>
		</button>
		<button id="logout" class="boton-vacio" data-placement="bottom" data-toggle="tooltip" title="Cerrar Sesion"><i class="fas fa-cog fa-lg" style="color: #161A20; font-size: 30px; margin-left: 10px;"></i></button>
		<a href="usuario_informacion.php">
		<button class="boton-vacio" href="usuario_inicio_sesion.php" data-placement="bottom" data-toggle="tooltip" title="Mi Usuario">
			<i class="fas fa-user fa-lg" style="color: #161A20; font-size: 30px; margin-left: 10px;"></i>
		</button>
		</a>
		<a href="usuario_carrito.php">
		<button class="boton-vacio" href="usuario_carrito.php" data-placement="bottom" data-toggle="tooltip" title="Carrito">
			<i class="fas fa-shopping-cart fa-lg" style="color: #161A20; font-size: 30px;"></i>
		</button>
		</a>
	</div>
<script src="js/bootstrap.js"></script>
<script src="js/bootstrap.bundle.js"></script>
<script src="js/logout.js"></script>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>
