<!DOCTYPE html>
<html>
<head>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap.bundle.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="js/index_header.js"></script>
	
	<link rel="icon" type="image/png" href="img/no.png"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<title>Inicio</title>
	<meta charset="utf-8">
</head>
<body>
	<header>
<?php
session_start();
if(!isset($_SESSION['usuario'])){
		//Usuario No Logeado
		include 'index_header.php';
	}
	else{
		//Usuario No Logeado
		include 'index_header_logeado.php';
	}
?>
	<script src="js/buscador_header.js"></script>
	</header>
	<main>
		<div  class="producto_targeta">
			<?php require "backend/home.php";
			foreach ($productos as $producto) { ?>
			<div id="producto">
				<div id="<?php echo $producto['id'] ?>" >
					<a href="producto_seleccionado.php?id=<?php echo $producto['id'] ?>"><img class="imagen_producto" src="img/<?php echo $producto['nombre'] ?>_1.png"></a>
					<strong> <?php echo $producto['nombre'] ?> </strong>
					<label>$<?php echo $producto['precio'] ?></label>
				</div>
			</div>
			<?php } ?>
		</div>
	</main>
	<footer>
		<?php // include 'backend/buscador_header.php'; ?>	
	</footer>
</body>
</html>