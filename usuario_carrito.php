<!DOCTYPE html>
<html>
<head>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/stock_carrito.js"></script>
  	<link rel="icon" type="image/png" href="img/icono.png"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">	
	<title>Mi Carrito</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<header> <!-- Encabezado -->
		<?php
		session_start();
		if(!isset($_SESSION['usuario'])){
				//Usuario no Logeado
				include 'index_header.php';
			}
			else{
				//Usuario Logeado
				include 'index_header_logeado.php';
			}
		?>
	</header>
		<script src="js/buscador_header.js"></script>
	<main>
		<?php if(isset($_GET['c'])) echo "<script type='text/javascript'> alert('Su compra se ha hecho correctamente'); </script> " ?>
		<div id="producto_target">			
			<?php require "backend/usuario_carrito.php"; ?>
			<h1>Mi Carrito</h1>
			<center><h3><?php if(isset($_GET['requisito'])){ echo $_GET['requisito'];}?></h3></center>
			<table class="table table-hover" id="cont_producto" style="text-align: center;">
				<tr class="table-primary">
					<th>Producto</th>
					<th>Nombre</th>
					<th>Precio</th>
					<th>Cantidad</th>
					<th>Precio Total</th>
					<th>Eliminar Todo <a href="backend/eliminar_producto_carrito.php"><i class="fas fa-ban" style="color:red;"></i></a></th>
				</tr>
				<?php 
				$total_de_todo=0;
				$productos_cant="";
				$productos_stock_total=0;
				if(isset($_SESSION['productos'])){
				foreach ($_SESSION['productos'] as $producto) {?>
					
					<tr class="table-primary" style="text-align: center;">
						
						<td id="img_td" class="producto_tabla_informacion"><a href="producto_seleccionado.php?id=<?php echo $producto['id'] ?>"><img class="img_car" src="img/<?php echo $producto['nombre'] ?>_1.png"></a></td>
						<td class="producto_tabla_informacion"><?php echo $producto['nombre'] ?> </td>
						<td class="producto_tabla_informacion">
							<div id= "precio_<?php echo $producto['id']?>">
								<?php echo $producto['precio'] ?>
							</div>
						</td>
						<td class="producto_tabla_informacion">
						<div id="cantidad">	
								<button class='menos variacion-canta-producto' n="<?php echo $producto['id']?>"><i class="fas fa-minus"></i></button>
								<input type='text' class='form-control variacion-canta-producto' id='text_cant_<?php echo $producto['id']?>' name='cant_producto' value="<?php echo $producto['cant_total']?>">
								<button class='mas variacion-canta-producto' n="<?php echo $producto['id']?>"><i class='fas fa-plus'></i></button>
						</div>
						<div id="cont_car" >
							<form method="POST" action="backend/verificacion_cantidad.php" >
								
								<input type="hidden" name="cant_stock" value="<?php echo $producto['stock'] ?>">
								<input type="hidden" name="id_producto" value="<?php echo $producto['id'] ?>">
								<input type="hidden" name="cantidad_de_productos" id="cant_product_hidden_<?php echo $producto['id']?>" value="1">
								<input type="hidden" id="cantiadad_de_stock_<?php echo $producto['id']?>" value="<?php echo $producto['stock'] ?>" >
							</form>			
						</div>
						</td>
						<td class="producto_tabla_informacion">
							<div id ="precio_total_<?php echo $producto['id']?>">
								<?php echo $producto['precio_total']?>
							</div>
						</td>
						<td class="producto_tabla_informacion"><a href="backend/eliminar_producto_carrito.php?id=<?php echo $producto['id'] ?>"><i class="fas fa-times" style="color: red;"></a></i></td>
					</tr>
						<?php $total_de_todo += intval($producto['precio_total']);  }} ?>
					

				</table>
			
			<center>
				<a href="index.php"><button class="btn btn-primary btnform">Seguir con la Compra</button></a>
				<a href="usuario_carrito.php"><button class="btn btn-primary btnform">Actualizar Datos</button></a>
				<a href="backend/enviar_compra_bd.php"><button class="btn btn-primary btnform">Comprar</button></a>
			</center>
			<h5>Total: $<?php echo $total_de_todo ?></h5>

		</main>
		</div>

	<footer>
		<div id="div"></div>
	</footer>
</body>
</html>