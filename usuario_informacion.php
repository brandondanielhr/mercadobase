<!DOCTYPE html>
<html>
<head>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap.bundle.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   	<script src="js/usuario_inicio_sesion_validacion.js"></script>
	<script src="js/add_product_index.js"></script>
    
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="icon" type="image/png" href="img/icono.png"/>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>Mi Usuario</title>
</head>
<body>
	<header>
		<?php require"index_header_logeado.php"; ?>
		<?php require 'backend/usuario_informacion.php' ?>
    	<script src="js/buscador_header.js"></script>
	</header>
	<main>
		<div  id="producto_target">
			<h1>Mi Sesion</h1>
	        <form method="POST">
			<table class="table table-hover" class="form_usuario">
				<thead>
				<tr>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Email</th>
					<th>Telefono</th>
					<th>Ciudad</th>
					<th>Calle</th>
	                <th>Acción</th>
				</tr>
				</thead>
				<tr class="table-primary">
						<td><?php echo($assoc['nombre']) ?></td>
						<td><?php echo($assoc['apellido']) ?></td>
						<td><?php echo($assoc['email']) ?></td>
						<td><?php echo($assoc['telefono']) ?></td>
						<td><?php echo($assoc['ciudad']) ?></td>
						<td><?php echo($assoc['calle']) ?></td>
						<td><a href="usuario_editar.php">Editar</a></td>
					</tr>
			</table>
	        </form>
        </div>
	</main>
	<footer>
	</footer>
	<script>
	$(document).ready(function(){
  		$('[data-toggle="tooltip"]').tooltip();   
	});
</script>
</body>
</html>
