<!DOCTYPE html>
<html>
<head>
	<script src="js/jquery.js"></script>
	<script src="js/usuario_inicio_sesion_validacion.js"></script>
	<link rel="icon" type="image/png" href="img/no.png"/>
	

	
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	
	<meta charset="utf-8">
	<title>Iniciar Sesion</title>
</head>
<body>
	
	<header>
		<?php include 'usuario_header.php' ?>
	</header>
	<main>
		<form id="login" class="form_usuario">
			<fieldset>
  				<legend>Ingresa tu Email y Contraseña:</legend>
  				<input type="hidden" name="id" value="<?php echo (isset($_COOKIE['id'])) ? $_COOKIE['id'] : "";?>">
  				<div class="form-group">
      				<label for="exampleInputEmail1">Email:</label>
      				<input type="email" name="email" class="form-control" id="exampleInputEmail1" value="<?php echo (isset($_COOKIE['email'])) ? $_COOKIE['email'] : "";?>" aria-describedby="emailHelp" placeholder="Escribir email..."
      				>
    			</div>
    			<div class="form-group">
      				<label for="exampleInputPassword1">Contraseña</label>
      				<input type="Password" name="password" class="form-control" id="exampleInputPassword1" value="<?php echo (isset($_COOKIE['password'])) ? $_COOKIE['password'] : "";?>" placeholder="Contraseña...">
    			</div>
    			<div>
    				Recordar <input type="checkbox" name="recordar" <?php echo (isset($_COOKIE['password'])) ? "checked" : "";?>>
    			</div>
  			</fieldset>
  			<center><input type="button" value="Enviar" class="btn btn-primary btnform" id="btn"></center>
		</form>
		<p>¿Todavia no tenes una cuenta? <a href="usuario_alta.php">Registrate aquí</a></p>
		<p>¿Olvidaste la contraseña?<a href="usuario_recuperacion_clave.php"> Recuperar </a></p>
	</main>
	<footer>
</body>
</html>