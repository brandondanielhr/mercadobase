<?php
session_start();
if(isset($_SESSION['usuario'])){
  header('location:index.php');
  }
?>
<!DOCTYPE html>
<html>
<head>
	<script src="js/jquery.js"></script>
  	<script src="js/bootstrap.js"></script>
	<script src="js/usuario_alta_validacion.js"></script>
	
  <link rel="icon" type="image/png" href="img/icono.png"/>
	<link rel="stylesheet" type="text/css" href="css/fontawesome/css/all.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<title>Registrarse</title>
	<meta charset="utf-8">

</head>
<body>
	<header> <!-- Encabezado -->
		<?php include 'usuario_header.php' ?>
	</header>
	<main> <!-- Cuerpo de la pagina -->


		<form id="formulario" class="form_usuario"> <!-- Formulario -->

			<fieldset>
  				<legend>Completa el siguiente formulario:</legend>
  				<div class="form-group">
      				<label for="exampleInputEmail1">Email:</label>
      				<input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Escribir email...">
              <div id="err_email" class="err_alert"></div>
    			</div>
    			<div class="form-group">
      				<label for="exampleInputEmail1">Nombre:</label>
      				<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre...">
              <div id="err_nombre" class="err_alert"></div>
    			</div>
    			<div class="form-group">
      				<label for="exampleInputEmail1">Apellido:</label>
      				<input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apellido...">
              <div id="err_apellido" class="err_alert"></div>
    			</div>
    			<div class="form-group">
      				<label for="exampleInputEmail1">Calle:</label>
      				<input type="text" class="form-control" name="calle" id="calle" placeholder="Calle...">
              <div id="err_calle" class="err_alert"></div>
    			</div>
    			<div class="form-group">
      				<label for="exampleInputEmail1">Ciudad:</label>
      				<input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ciudad...">
              <div id="err_ciudad" class="err_alert"></div>
    			</div>
          <div class="form-group">
              <label for="exampleInputEmail1">Telefono:</label>
                <input type="number" class="form-control" name="telefono" id="telefono" placeholder="Telefono..." pattern="[0-9]" title="Agregue numero de telefono mayor de 9 numeros" >
                <div id="err_telefono" class="err_alert"></div>
          </div>
    			<div class="form-group">
      				<label for="exampleInputPassword1">Contraseña</label>
      				<input type="Password" class="form-control " name="password" id="contrasena" placeholder="Contraseña..." pattern="[a-zA-Z0-9]{6,15}" title="La contraseña debe de tener letras y/o numeros y tener una logitud entre 6 y 15 caracteres">
              <div id="err_contrasena" class="err_alert"></div>
    			</div>
              	<label id="showpass" for="remember-me">Mostrar Contraseña</label>
              	<input type="checkbox" id="mostrar">
    			<div class="form-group">
      				<label for="exampleInputPassword1">Confirmar Contraseña</label>
      				<input type="Password" class="form-control" id="contrasena_again" name="contrasena_again" placeholder="Confirmar Contraseña...">
              <div id="err_contrasena_again" class="err_alert"></div>
    			</div>
  			</fieldset>
  			<center><input type="button" class="btn btn-primary btnform" id="btn" value="Enviar"></input></center>
		</form>
    <p>¿Ya tienes una cuenta? <a href="usuario_inicio_sesion.php">Inicia Sesion aquí</a></p>
	</main>
	<footer>
	</footer>
</body>
</html>